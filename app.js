var app = angular.module("myapp", ['ngRoute']);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/test/:testId", {
			templateUrl: "test.html",
			controller: "testController"
		})
		.when("/finish", {
			templateUrl: "finish.html",
			controller: "finishController"
		})
		.when("/home", {
			templateUrl: "home.html",
			controller: "mainController"
		})
		.otherwise({ redirectTo: '/home' });
});

app.controller("mainController", function ($scope, shareData, $http) {
	console.log("controller");

	$http.get('http://localhost:3000/hi')
		.then(function (response) {
			console.log(response);
			$scope.data = response.data.data;
			shareData.setData($scope.data);
		})
		.catch(function (err) {
			$scope.data = [
				{
					"name": "JS",
					"questions": [{
						"text": "JS first question",
						"options": ["abc", "xyz", "pqr"],
						"correct": 1
					},
					{
						"text": "JS second question",
						"options": ["qqq", "ccc", "sdffdg"],
						"correct": 0
					}]
				},
				{
					"name": "Node",
					"questions": [{
						"text": "Node first question",
						"options": ["abc", "xyz", "pqr"],
						"correct": 2
					},
					{
						"text": "Node second question",
						"options": ["qqq", "ccc", "sdffdg"],
						"correct": 1
					},
					{
						"text": "Node third question",
						"options": ["jjj", "kjj", "nnn"],
						"correct": 0
					}]
				},
				{
					"name": "Java",
					"questions": [{
						"text": "Java first question",
						"options": ["abc", "xyz", "pqr"],
						"correct": 1
					},
					{
						"text": "Java second question",
						"options": ["qqq", "ccc", "sdffdg"],
						"correct": 1
					}]
				}
			];
			shareData.setData($scope.data);
		})

})

app.factory("shareData", function () {
	var allData = [];
	var result = {};

	return {
		setData: function (data) {
			allData = data;
		},
		getData: function () {
			return allData;
		},
		setResult: function (data) {
			result = data;
		},
		getResult: function () {
			return result;
		}
	}
})

app.controller("testController", function ($scope, shareData, $routeParams, $location) {
	console.log("test controller");
	console.log($routeParams.testId);

	// $scope.testId = $routeParams.testId;
	// $scope.allData = shareData.getData()
	// $scope.selectedTest = $scope.allData[$scope.testId];

	$scope.selectedTest = shareData.getData()[$routeParams.testId];
	console.log($scope.selectedTest);

	$scope.currentQuestion = 0;

	$scope.result = {}
	$scope.result.testName = $scope.selectedTest.name;
	$scope.result.totalQuestions = $scope.selectedTest.questions.length;
	$scope.result.quesAnsArray = [];

	for (var i = 0; i < $scope.selectedTest.questions.length; i++) {
		$scope.result.quesAnsArray.push({ "correctOption": $scope.selectedTest.questions[i].correct })
	}

	$scope.saveSelectedOption = function (index) {
		console.log(index)
		if (index == $scope.selectedTest.questions[$scope.currentQuestion].correct) {
			$scope.result.quesAnsArray[$scope.currentQuestion].flag = true;
		}
		else {
			$scope.result.quesAnsArray[$scope.currentQuestion].flag = false;
		}
	}

	$scope.saveResult = function () {
		console.log($scope.result)
		shareData.setResult($scope.result);
		$location.path('finish');
	}
})

app.controller("finishController", function ($scope, shareData) {
	console.log(shareData.getResult())
	$scope.result = shareData.getResult();

	$scope.result.correctCount = 0;
	for (var i = 0; i < $scope.result.quesAnsArray.length; i++) {
		if ($scope.result.quesAnsArray[i].flag) {
			$scope.result.correctCount++;
		}
	}
	console.log($scope.result)
})